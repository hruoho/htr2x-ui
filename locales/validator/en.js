export const messages = {
  _default: (field) => `This value is not valid.`,
  after: (field, [target, inclusion]) => `This must be after ${inclusion ? 'or equal to ' : ''}${target}.`,
  alpha_dash: (field) => `This may contain alpha-numeric characters as well as dashes and underscores.`,
  alpha_num: (field) => `This may only contain alpha-numeric characters.`,
  alpha_spaces: (field) => `This may only contain alphabetic characters as well as spaces.`,
  alpha: (field) => `This may only contain alphabetic characters.`,
  before: (field, [target, inclusion]) => `This must be before ${inclusion ? 'or equal to ' : ''}${target}.`,
  between: (field, [min, max]) => `This must be between ${min} and ${max}.`,
  confirmed: (field) => `This confirmation does not match.`,
  credit_card: (field) => `This is invalid.`,
  date_between: (field, [min, max]) => `This must be between ${min} and ${max}.`,
  date_format: (field, [format]) => `This must be in the format ${format}.`,
  decimal: (field, [decimals] = ['*']) => `This must be numeric and may contain ${!decimals || decimals === '*' ? '' : decimals} decimal points.`,
  digits: (field, [length]) => `This must be numeric and exactly contain ${length} digits.`,
  dimensions: (field, [width, height]) => `This must be ${width} pixels by ${height} pixels.`,
  email: (field) => `This must be a valid email.`,
  ext: (field) => `This must be a valid file.`,
  image: (field) => `This must be an image.`,
  in: (field) => `This must be a valid value.`,
  integer: (field) => `This must be an integer.`,
  ip: (field) => `This must be a valid ip address.`,
  length: (field, [length, max]) => {
    if (max) {
      return `This length be between ${length} and ${max}.`;
    }

    return `This length must be ${length}.`;
  },
  max: (field, [length]) => `This may not be greater than ${length} characters.`,
  max_value: (field, [max]) => `This must be ${max} or less.`,
  mimes: (field) => `This must have a valid file type.`,
  min: (field, [length]) => `This must be at least ${length} characters.`,
  min_value: (field, [min]) => `This must be ${min} or more.`,
  not_in: (field) => `This must be a valid value.`,
  numeric: (field) => `This may only contain numeric characters.`,
  regex: (field) => `This format is invalid.`,
  required: (field) => `This is required.`,
  size: (field, [size]) => `The size must be smaller.`,
  url: (field) => `This is not a valid URL.`
};

export default {messages}
