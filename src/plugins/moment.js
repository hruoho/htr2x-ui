// import moment from 'moment'

const moment = require('moment')
require('moment-duration-format')
let plugin = {
  install (Vue, options) {
    Vue.prototype.$moment = moment

    Vue.prototype.$fmtDate = (time) => {
      let fmt = 'DD/MM/YYYY HH:mm:ss'
      if (!moment(time).isValid()) return time
      return moment.utc(time).format(fmt)
    }

    Vue.prototype.$fmtTime = (time) => {
      let fmt = 'HH:mm:ss'
      if (!moment(time).isValid()) return time
      return moment.utc(time).format(fmt)
    }

    Vue.prototype.$fmtDuration = (dur) => {
      let fmt = 'hh:mm:ss'
      if (!dur) return dur
      return moment.duration(dur).format(fmt)
    }
  }
}

export default plugin
