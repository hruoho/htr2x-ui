// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import VueResource from 'vue-resource'
import moment from './plugins/moment'
import VeeValidate from 'vee-validate'
import dict from '../locales/validator/en' // forked default validation strings
import 'bootstrap/dist/css/bootstrap.min.css'
Vue.use(VueResource)

Vue.http.options.root = 'http://localhost:3030'

Vue.router = router // new Router(...)

Vue.use(require('@websanova/vue-auth'), {
  auth: require('./jwt.js'),
  // auth: require('@websanova/vue-auth/drivers/auth/basic.js'),
  http: require('@websanova/vue-auth/drivers/http/vue-resource.1.x.js'),
  router: require('@websanova/vue-auth/drivers/router/vue-router.2.x.js'),
  refreshData: {url: 'authentication', method: 'POST', fetchUser: false, enabled: true, interval: 1}
})

Vue.use(moment)

Vue.use(VeeValidate, {
  classes: true,
  classNames: {
    valid: 'is-valid', // model is valid
    invalid: 'is-invalid' // model is invalid
  },
  validity: false,
  dictionary: {
    en: dict
  }
})

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App }
})
