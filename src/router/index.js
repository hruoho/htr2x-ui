import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Results from '@/components/Results'
import Entries from '@/components/Entries'
import Fleets from '@/components/Fleets'
import EntryForm from '@/components/EntryForm'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/results',
      name: 'Results',
      component: Results
    },
    {
      path: '/entries',
      name: 'Entries',
      component: Entries
    },
    {
      path: '/fleets',
      name: 'Fleets',
      component: Fleets
    },
    {
      path: '/enter',
      name: 'Entry form',
      component: EntryForm
    }
  ],
  linkExactActiveClass: 'active'
})
